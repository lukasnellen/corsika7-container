#! /bin/sh

#set -x
set -e

HE=QGSII
LE=urqmd

for i; do
    case $i in
	qgsjet) HE=QGSII ;;
	sibyll) HE=SIBYLL ;;
	epos) HE=EPOS ;;
	fluka|flukainfn) LE=flukainfn ;;
	urqmd) LE=urqmd ;;
    esac
done

exec /opt/corsika/run/corsika${CORSIKA}Linux_${HE}_${LE}
