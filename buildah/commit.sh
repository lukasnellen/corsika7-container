#! /bin/sh

#set -x
set -e

DIR=$(readlink -f $(dirname $0))

. ${DIR}/conf.sh

buildah commit ${container} corsika7/c7:${CORSIKA}
buildah tag corsika7/c7:${CORSIKA} corsika7/c7
