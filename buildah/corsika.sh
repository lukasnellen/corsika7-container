#! /bin/sh

#set -x
set -e

DIR=$(readlink -f $(dirname $0))

. ${DIR}/conf.sh

buildah run ${container} mkdir -p /opt/scripts
zcat ${SRCDIR}/${CORSIKASRC} | buildah run ${container} bash -c "cd /opt; tar xvf -; mv corsika-${CORSIKA} corsika"
buildah copy ${container} run-corsika.sh /opt/scripts
buildah config --env CORSIKA=${CORSIKA} --entrypoint /opt/scripts/run-corsika.sh ${container}
