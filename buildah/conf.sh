# source me
SRCDIR="${HOME}/CORSIKA/src"
FLUKA="fluka2021.2-linux-gfor64bitAA.tar.gz"
FLUKADAT="fluka2021.2-data.tar.gz"
CORSIKA="77500"
CORSIKASRC="corsika-${CORSIKA}.tar.gz"
container="c7base:u-22.04"
