#! /bin/sh
# unpack and build fluka

#set -x
set -e

DIR=$(readlink -f $(dirname $0))

. ${DIR}/conf.sh

buildah run ${container} mkdir -p /opt/fluka
zcat ${SRCDIR}/${FLUKA} | buildah run ${container} bash -c 'cd /opt/fluka; tar xvf -'
zcat ${SRCDIR}/${FLUKADAT} | buildah run ${container} bash -c 'cd /opt/fluka; tar xvf -'
buildah run ${container} chmod -R a+r /opt/fluka
buildah config --env FLUPRO=/opt/fluka --env FLUFOR=gfortran ${container}
buildah run ${container} make -C /opt/fluka SHELL=/bin/bash
