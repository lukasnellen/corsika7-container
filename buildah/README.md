CORSIKA 7 container building
============================

## Preparation

These tools use the general [container base] tools to create a container with distribution specific packages, for example compilers.

We currently base our containers on ubuntu 22.04, but other base distributions and versions are supported by the [container base] tools.

## Base container

First make sure that you have a `project.sh` file that set the `project` variable to _corsika7_.

```sh
$ cat project.sh
# currently active project or placeholder
project=corsika7
```

Alternatively, you can set the environment variable `PROJECT_NAME`:

```sh
$ export PROJECT_NAME=corsika7
```

Now you can build the base container:

```sh
./sysdev-prepare u 22.04
./run-command.sh -i u-22.04 corsika7
./buildah-commit
./remove-all
```

## Add fluka and corsika 7 to the container

To create buildah container from saved base and add the corisika 7 and fluka code, run

```
./prepare.sh
./corsika.sh c7base:u-22.04
./fluka.sh c7base:u-22.04
```

## Build corsika
To build CORSIKA, you have to run `coconut` in the container:
```
buildah run c7base:u-22.04 bash
cd /opt/corsika
./coconut
```

Possible combinations could be

- epos + urqmd
- epos + fluka
- qgsjet II + urqmd
- sibyll + fluka

The entrypoint script process the names of interaction models passed as arguments to determine the binary to execute.

## Commit container

```
./commit.sh
```

This will generate both a versions and a `latest` container tag.