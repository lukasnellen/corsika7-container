Container builder for CORSIKA 7
===============================

Containers are build in three stages. The first two stages use [buildah] to build [podman] containers. In the third step, tje [podman] container is converted to an [apptainer] container. 

1) Build a base image. We currently use Ubuntu 22.04 (LTS) as the base system. See [container-base] for details.
2) Install [fluka] (if desired) and [CORSIKA 7] into the container. Note that you have to buld [CORSIKA 7] manually in the container. See the information in the `buildah` subdirectory.
3) Create the apptainer container, here named `c7.sif`.

[buildah]: https://buildah.io
[podman]: https://podman.io
[apptainer]: https://apptainer.org
[container-base]: https://gitlab.com/lukasnellen/container-base

[fluka]: http://www.fluka.org
[CORSIKA 7]: https://www.iap.kit.edu/corsika
