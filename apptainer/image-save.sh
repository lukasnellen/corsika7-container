#! /bin/sh

set -e

base=$1
tag=$2

podman image save --format=oci-archive --output=${base}-${tag}.tar corsika7/${base}:${tag}
