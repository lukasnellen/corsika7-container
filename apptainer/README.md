Build the final container in two steps:

1) Export the podman container to OCI using `./image-save.sh c7 latest`
2) Build the container using `image-build.sh`

You can run the container directly, which will execute one of the pre-built CORSIKA binaries inside. We provide example inputs for CORSIKA build with the EPOS or QGSJET II interaction models. The paths in the examples are adjusted to work with the container. The runsript picks the binary based on the interaction models requested. For example:

```
mkdir -p run/test
cd run/test
../../c7.sif sibyll fluka < ../../sample-inputs/all-inputs
```
